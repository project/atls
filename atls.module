<?php

/**
 * @file
 * Primary module hooks for Admin Toolbar Language Selector module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function atls_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.atls':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Provides a dropdown language select box within the admin toolbar') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_toolbar().
 */
function atls_toolbar() {
  $user = \Drupal::currentUser();

  $items = [];
  $items['shortcuts'] = [
    '#cache' => [
      'contexts' => [
        'user.permissions',
      ],
    ],
  ];

  if ($user->hasPermission('access atls')) {
    $is_admin = Drupal::service('router.admin_context')->isAdminRoute();
    $languages = Drupal::service('language_manager')->getLanguages();
    $settings = Drupal::config("atls.settings");
    $items['lang'] = [
      '#type' => 'toolbar_item',
      'tab' => [
        '#type' => 'select',
        '#options' => array_map(function ($l) use ($settings) {
          return $settings->get("is_langcode") ? $settings->get("is_uppercase") ? strtoupper($l->getId()) : $l->getId() : $l->getName();
        }, $languages),
        '#attributes' => [
          'class' => ['toolbar-lang-select' . ($is_admin ? '-admin' : '')],
          'id' => 'toolbar-lang-select',
        ],
      ],
      '#wrapper_attributes' => [
        'class' => ['atls-toolbar-tab-' . $settings->get("position")],
      ],
      '#attached' => [
        'library' => ['atls/atls'],
        'drupalSettings' => [
          'language' => [
            'pefixes' => Drupal::config('language.negotiation')->get('url.prefixes'),
          ],
        ],
      ],
      '#weight' => $settings->get("weight"),
    ];
  }

  return $items;
}
