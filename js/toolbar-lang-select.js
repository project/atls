Drupal.behaviors.atlsBehavior = {
  attach: function (context, settings) {
    var select = document.getElementById("toolbar-lang-select");
    select.selectedIndex = 0;
    select.value = drupalSettings.path.currentLanguage;
    select.addEventListener('change', (event) => {
      var lang = event.target.value;
      var prefix = drupalSettings.language.pefixes[lang];
      if (prefix !== "") {
        prefix += "/";
      }
      var path = drupalSettings.path;
      window.location = path.baseUrl + prefix + path.currentPath;
    });
  }
};
