<?php

namespace Drupal\atls\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Admin Toolbar Language Selector settings for the site.
 */
class AtlsSettingsForm extends ConfigFormBase {

  /**
   * Render cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheRender;

  /**
   * Class constructor.
   */
  public function __construct(CacheBackendInterface $cache_render) {
    $this->cacheRender = $cache_render;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.render')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'atls_atls_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['atls.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['weight'] = [
      '#type' => 'number',
      '#title' => $this->t('Weight'),
      '#description' => $this->t("Left positioned selector with lower weight will move to the left of the list,</br> while heavier weight will move to the right (v.v. for Right positioned selector)."),
      '#default_value' => $this->config('atls.settings')->get('weight'),
    ];

    $form['position'] = [
      '#type' => 'radios',
      '#title' => $this->t('Position'),
      '#options' => [
        'left' => $this->t("Left"),
        'right' => $this->t("Right"),
      ],
      '#default_value' => $this->config('atls.settings')->get('position'),
    ];

    $form['is_langcode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Format'),
      '#options' => [
        0 => $this->t("Name"),
        1 => $this->t("Language Code"),
      ],
      '#default_value' => $this->config('atls.settings')->get('is_langcode'),
    ];

    $form['is_uppercase'] = [
      '#type' => 'radios',
      '#title' => $this->t('Capitalization of the language code'),
      '#options' => [
        0 => $this->t("Lower case"),
        1 => $this->t("Upper case"),
      ],
      '#default_value' => $this->config('atls.settings')->get('is_uppercase'),
      '#states' => [
        'visible' => [
          ':input[name="is_langcode"]' => ['value' => '1'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('atls.settings')
      ->set('weight', $form_state->getValue('weight'))
      ->set('position', $form_state->getValue('position'))
      ->set('is_langcode', $form_state->getValue('is_langcode'))
      ->set('is_uppercase', $form_state->getValue('is_uppercase'))
      ->save();

    $this->cacheRender->invalidateAll();

    parent::submitForm($form, $form_state);
  }

}
